import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CompanyServiceService } from './company-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  companies = null

  constructor(private service: CompanyServiceService) {
  }

   getCompanies() {
    this.companies =  this.service.requestCompanies().then(response => {
      this.companies = response
    })
  }

}
