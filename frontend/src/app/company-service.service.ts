import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompanyServiceService {
  apiURL = "";
  response = null

  constructor(private http: HttpClient) {
    this.apiURL = 'http://localhost:8080';
  }

  requestCompanies() {
    return this.http.get(`${this.apiURL}/companies`)
      .toPromise()
     
  }

}
