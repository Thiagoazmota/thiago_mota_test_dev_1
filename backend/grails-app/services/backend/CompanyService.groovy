package backend
import java.time.LocalDateTime
import grails.gorm.transactions.Transactional
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import groovy.json.*


@Transactional
class CompanyService {

    def getStocks(String company, long numbersOfHoursUntilNow) {
        long startTime = System.currentTimeMillis();

            Company company_ = Company.where{ name == company }.get()

            if(Objects.isNull(company_)){
                println "Company not found"
            } else {
                LocalDateTime initialHour = LocalDateTime.now().minus(numbersOfHoursUntilNow, ChronoUnit.HOURS)
                println initialHour
                def stocks = Stock.findAll{ (date >= initialHour) && (company == company_) }

                println "\n\n-----------------------------------------"
                println stocks.size()  + " stock prices were found for company " + company_.name + " in last " + numbersOfHoursUntilNow + " hours\n"; 

                stocks.each {
                    println "Price: " + it.price
                    println "Data: " + it.date 
                }
                
                long endTime = System.currentTimeMillis();

                println "\nThat took " + (endTime - startTime) + " milliseconds";
                println "-----------------------------------------\n\n"
            }

        }

    def getCompanies() {
        return Company.findAll{}
    }
}
 