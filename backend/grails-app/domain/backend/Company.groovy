package backend

class Company {
    String name
    String segment
    static hasMany = [stocks: Stock]


    static constraints = {
        name blank: false, nullable: false
        segment blank: false, nullable: false
    }

    static mapping = {
        version false
    }
}
