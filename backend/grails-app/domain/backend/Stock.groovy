package backend
import java.time.LocalDateTime

class Stock {
    float price
    LocalDateTime date
    Company company
    static belongsTo = [company: Company]


    static constraints = {
        price blank: false
        date nullable: false
    }

    static mapping = {
        version false
    }
}
