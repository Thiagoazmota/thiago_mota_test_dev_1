package backend
import java.time.temporal.ChronoUnit;
import java.time.LocalDate
import java.time.LocalTime
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.text.SimpleDateFormat
import java.text.DateFormat

class BootStrap {
    def CompanyService
    def init = { servletContext ->
        Random rand = new Random()
        Company[] companies = new Company[3]

        final BASE_PRICE_UBER =115.0
        final BASE_PRICE_AMAZON = 80.0
        final BASE_PRICE_FORD = 15.0

        Company companyAmazon = new Company(name:"AMAZON", segment:"tech").save()
        Company companyFord = new Company(name:"FORD", segment:"auto").save()
        Company companyUber = new Company(name:"UBER", segment:"transport").save()

        companies[0] = companyAmazon
        companies[1] = companyFord
        companies[2] = companyUber

        LocalDate actualDate = LocalDate.now();
        LocalDate endDate = actualDate.minus(30, ChronoUnit.DAYS)

        LocalTime openMarket = LocalTime.parse("10:00:00", DateTimeFormatter.ISO_TIME); 
        LocalTime closeMarket = LocalTime.parse("18:00:00", DateTimeFormatter.ISO_TIME);

        println "Please wait, inserting data..."
        for (LocalDate date = endDate; date <= actualDate; date = date.plusDays(1)){
            LocalTime aux = openMarket
            while(aux <= closeMarket){
                LocalDateTime finalStockDateTimeVariation = LocalDateTime.of(date, aux)

                new Stock(company: companies[0], price: BASE_PRICE_FORD + ((BASE_PRICE_FORD * 0.03) * rand.nextFloat()), date: java.sql.Timestamp.valueOf(finalStockDateTimeVariation)).save()
                new Stock(company: companies[1], price: BASE_PRICE_AMAZON + ((BASE_PRICE_AMAZON * 0.03) * rand.nextFloat()), date: java.sql.Timestamp.valueOf(finalStockDateTimeVariation)).save()
                new Stock(company: companies[2], price: BASE_PRICE_UBER + ((BASE_PRICE_UBER * 0.03) * rand.nextFloat()), date: java.sql.Timestamp.valueOf(finalStockDateTimeVariation)).save()
                aux = aux.plusMinutes(1)
            }
            
        }
            
        CompanyService.getStocks('FORD', 5)
    }
    def destroy = {
    }
} 
 