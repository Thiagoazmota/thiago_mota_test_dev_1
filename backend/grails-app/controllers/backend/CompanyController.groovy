package backend
import groovy.json.JsonOutput 
import java.util.*;

class CompanyController {

    static responseFormats = ['json']
    def CompanyService

    def getCompanies() {
        def objetos
        ArrayList<Properties> p_list = new ArrayList<Properties>();
        CompanyService.getCompanies().each{
            int i = 0
            Properties p = new Properties();
            def companyAux = Company.findById(it.id)

            def stocks = Stock.findAll{company == companyAux}
            float[] stock_prices = new float[stocks.size()]
            
            stocks.each {
                stock_prices[i++] = it.price
            }

            
            def sd = calculateSD(stock_prices)


            p.put("name", it.name);
            p.put("segment", it.segment);
            p.put("standard_deviation", sd);
            p_list.add(p) 
        }
        render JsonOutput.toJson(p_list) 
    }

  
    def calculateSD(float[] numArray)
    {
        float sum = 0.0, standardDeviation = 0.0
        int length = numArray.length

        for(float num : numArray) {
            sum += num
        }

        float mean = sum/length

        for(float num: numArray) {
            standardDeviation += Math.pow(num - mean, 2)
        }

        return Math.sqrt(standardDeviation/length)
    }
}
 